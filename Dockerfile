FROM rust:latest

WORKDIR /usr/src/ruslang-rocket-mongodb

COPY . .

RUN cargo build --release

RUN cargo install --path .

CMD ["/usr/local/cargo/bin/ruslang-rocket-mongodb"]